# Sync your OSCAR fork
This project provides an easy way to sync your personal OSCAR fork
to the main repository.

## Getting started
Follow these steps once to set up your sync:

1. [Fork this repository](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html#creating-a-fork)
and continue reading these instructions in your new fork. Note that you will need to configure GitLab settings in both your OSCAR fork (step 4) and your fork of this project (step 5). Be sure you are in the correct project for each step.

2. If you know that the ssh key you use with GitLab is not encrypted with a passphrase, you can skip to step 5.

3. Create a ssh key pair without encrypting the private key (no passphrase), e.g. in a command-line shell with `ssh-keygen -f my-unencrypted-key -C "sync-fork-to-upstream" -N ""` (which puts the private key and public key in separate files named `my-unencrypted-key` and `my-unencrypted-key.pub`), or you can [use this web page](https://js-keygen.surge.sh/) (this is a bit risky but it appears that the keys do not leave your browser).

4. On the GitLab page of your OSCAR fork, add your new key as a [project Deploy Key](https://docs.gitlab.com/ee/user/project/deploy_keys/#create-a-project-deploy-key):
   * Bring up the main page of your OSCAR fork in your browser.
   * From the left sidebar, select Settings > Repository (gear icon).
   * Expand Deploy keys.
   * Enter anything in the Title area (e.g. `sync-fork-to-upstream`).
   * In the Key area paste your public key (should start with `ssh-`).
   * Enable the "Grant write permissions to this key" checkbox.
   * Click the "Add key" button.

5. On the GitLab page of your sync-fork-to-upstream fork (i.e. *not* your OSCAR fork), assign a [project CI/CD variable](https://docs.gitlab.com/ee/user/project/deploy_keys/#create-a-project-deploy-key) to your private key:
   * From the left sidebar, select Settings > CI/CD (gear icon).
   * Expand Variables.
   * Click the "Add variable" button.
   * In the Key area enter `GIT_SSH_PRIVATE_KEY`
   * In the Value area paste your private key (typically starts with `----BEGIN OPENSSH PRIVATE KEY-----`).
   * Click the "Add variable" button.

## Doing a sync

On the GitLab page of your sync-fork-to-upstream fork (i.e. *not* your OSCAR fork):

1. From the left sidebar, select CI/CD > Pipelines. You could bookmark the destination page to skip this step in the future.

2. Click the "Run pipeline" button.

3. Keep all the defaults and click the "Run pipeline" button.

To view the status of your sync, select CI/CD > Jobs from the left sidebar (rocketship icon) and click the top-most Status badge. A successful sync should look something [like this](https://gitlab.com/rhashimoto/sync-fork-to-upstream/-/jobs/3313985257).
