#!/bin/bash

FORK_REPO="git@gitlab.com:${CI_PROJECT_ROOT_NAMESPACE}/OSCAR-code.git"
UPSTREAM="https://gitlab.com/pholy/OSCAR-code.git"

# Clone the user's fork.
git clone $FORK_REPO repo
cd repo

# Set the upstream repo.
git remote add upstream $UPSTREAM

# Merge fork clone with upstream.
git pull --ff-only upstream master

# Push changes to the fork.
git push origin master
